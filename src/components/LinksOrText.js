import React from "react";
import _ from "lodash";
import { Link } from "react-router-dom";

export default ({ urlAndNameList, info }) => {
  if (!info || !info.length || !info[0]) return "-";

  const infoArray = _.isArray(info) ? info : [info];

  return _.map(infoArray, value => {
    const withUrlAndName = _.find(urlAndNameList, { url: value });

    if (!withUrlAndName) return value + "  ";

    const [id, type] = withUrlAndName.url.split("/").reverse();
    return (
      <span>
        <Link to={"/details/" + type + "/" + id}>{withUrlAndName.name}</Link>{"  "}
      </span>
    );
  });
};
