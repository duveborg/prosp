import React, { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import LinksOrText from "../components/LinksOrText";
import _ from "lodash";
import "./Details.css";

const fetchNameFromApiUrl = async url => {
  const response = await fetch(url);
  const { name } = await response.json();
  return {
    name,
    url
  };
};

const fetchNamesFromUrls = async infoList => {
  const promises = _(infoList)
    .values()
    .flatten()
    .value()
    .filter(info => info.toString().startsWith("http"))
    .map(fetchNameFromApiUrl);

  const urlAndNameList = await Promise.all(promises);
  return {
    urlAndNameList,
    infoList
  };
};

const Details = () => {
  const { type, id } = useParams();
  const [apiResponse, setApiResponse] = useState({});

  useEffect(() => {
    setApiResponse({});
    fetch("https://anapioficeandfire.com/api/" + type + "/" + id)
      .then(response => response.json())
      .then(fetchNamesFromUrls)
      .then(setApiResponse);
  }, [type, id]);

  return (
    <div>
      <Link to="/">Back to Houses list</Link>
      <h3>Details</h3>

      {_.isEmpty(apiResponse.infoList) && "Loading..."}

      <table className="Details-table">
        <tbody>
          {_.map(apiResponse.infoList, (info, key) => (
            <tr key={key}>
              <td>{key}</td>
              <td>
                <LinksOrText
                  urlAndNameList={apiResponse.urlAndNameList}
                  info={info}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Details;
