import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import parseLinkHeader from "parse-link-header";
import "./List.css";

const List = () => {
  const [houses, setHouses] = useState([]);
  const [paginationLinks, setPaginationLinks] = useState({});

  const fetchHouses = apiUrl => {
    // for some reason, this resource doesnt have the same cors headers as the others
    const corsProxy = "https://cors-anywhere.herokuapp.com/";
    fetch(corsProxy + apiUrl).then(async response => {
      const links = parseLinkHeader(response.headers.get("link"));
      setPaginationLinks(links);

      const houses = await response.json();
      setHouses(houses);
    });
  };

  useEffect(() => fetchHouses("https://anapioficeandfire.com/api/houses?pageSize=25"), []);

  return (
    <div>
      <h3>List of houses</h3>
      {houses.map(house => {
        const [houseId] = house.url.split("/").reverse();
        return (
          <div className="List-item">
            <Link to={"/details/houses/" + houseId}>{house.name}</Link>
          </div>
        );
      })}

      <div className="List-pagination">
        {[
          ["first", "First page"],
          ["prev", "Previous page"],
          ["next", "Next page"],
          ["last", "Last page"]
        ].map(
          ([property, label]) =>
            paginationLinks[property] && (
              <button onClick={() => fetchHouses(paginationLinks[property].url)}>
                {label}
              </button>
            )
        )}
      </div>
    </div>
  );
};

export default List;
